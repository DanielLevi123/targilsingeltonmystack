﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TargilSingelton
{
    class MyStack
    {
        private List<int> m_singelton_list;
        private System.Collections.Concurrent.ConcurrentQueue<int> m_conncurrent;
        private MyStack()
        {

        }
        private static MyStack Instance;
        private static object key = new object();
        public static MyStack GetInstance()
        {
            if (Instance == null)
            {
                lock (key)
                {
                    if (Instance == null)
                    {
                        Instance = new MyStack();
                    }
                }
            }
            return Instance;
        }
        public void PushWithLock(int item)
        {
            lock (key)
            {
                m_singelton_list.Insert(0, item);
                Console.WriteLine($"Push {item}");
            }
        }
        public int PopWithLock()
        {
            int result = 0;
            lock (key)
            {
                result = m_singelton_list[m_singelton_list.Count - 1];
                m_singelton_list.RemoveAt(0);
                Console.WriteLine($"Remove {result}");
            }
            return result;
        }
        public int PeepWithLock()
        {
            int result = 0;
            lock (key)
            {
                result = m_singelton_list[m_singelton_list.Count - 1];
                Console.WriteLine($"Peep {result}");
            }
            return result;
        }
        public void PushWithMonitor(int item)
        {
            try
            {
                Monitor.Enter(key);
                m_singelton_list.Insert(0, item);
                Console.WriteLine($"Push {item}");
            }
            finally
            {
                Monitor.Exit(key);
            }
        }
        public int PopWithMonitor()
        {
            int result = 0;
            try
            {
                Monitor.Enter(key);
                result = m_singelton_list[m_singelton_list.Count - 1];
                m_singelton_list.RemoveAt(0);
                Console.WriteLine($"Remove {result}");
            }
            finally
            {
                Monitor.Exit(key);
            }
            return result;
        }
        public int PeepWithMonitor()
        {
            int result = 0;
            try
            {
                Monitor.Enter(key);
                result = m_singelton_list[m_singelton_list.Count - 1];
                Console.WriteLine($"Peep {result}");
            }
            finally
            {
                Monitor.Exit(key);
            }
            return result;
        }
        public void PushWithConncurrent(int item)
        {
            
                m_conncurrent.Enqueue(item);
                Console.WriteLine($"Push {item}");
            
        }
        public int PopWithConncurrent()
        {
            int result;
             m_conncurrent.TryDequeue(out result);
            Console.WriteLine($"Remove {result}");
            return result;
        }
        public int PeepWithConncurrent()
        {
            int result;
            m_conncurrent.TryPeek(out result);
            Console.WriteLine($"Peep {result}");   
            return result;
        }
    }
}




